﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Cloud.Sample.Core.Models;
using Umbraco.Web.Mvc;
namespace Umbraco.Cloud.Sample.Core.Controllers
{
    public class MovieSurfaceController : SurfaceController
    {
        public ActionResult Index()
        {
            return PartialView("MovieForm", new Movie());
        }
        [HttpPost]
        public ActionResult HandleUpdateMovieForm(Movie m)
        {
            return RedirectToCurrentUmbracoPage();
        }
    }
}
